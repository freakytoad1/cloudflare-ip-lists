package main

import (
	"log"

	"gitlab.com/freakytoad1/cloudflare-ip-lists/sourcelist"
)

func main() {
	log.Printf("hello world!")

	sourceLists := []string{
		"https://www.spamhaus.org/drop/edrop.txt",
		"https://www.spamhaus.org/drop/drop.txt",
		"https://feodotracker.abuse.ch/downloads/ipblocklist_recommended.txt",
		"https://raw.githubusercontent.com/ktsaou/blocklist-ipsets/master/firehol_level1.netset",
	}

	for _, source := range sourceLists {
		parsedSource, err := sourcelist.Retrieve(source)
		if err != nil {
			log.Printf("unable to retrieve source list: %v", err)
		}

		log.Printf("%q provided %d entries", source, len(parsedSource))
	}
}
