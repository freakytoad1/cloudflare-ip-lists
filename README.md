# cloudflare-ip-lists [![pipeline status](https://gitlab.com/freakytoad1/cloudflare-ip-lists/badges/main/pipeline.svg)](https://gitlab.com/freakytoad1/cloudflare-ip-lists/-/commits/main) [![coverage report](https://gitlab.com/freakytoad1/cloudflare-ip-lists/badges/main/coverage.svg)](https://gitlab.com/freakytoad1/cloudflare-ip-lists/-/commits/main)

The intent of this project is to provide an easy way to update Cloudflare IP Lists via the Cloudflare API. These IP Lists can be used in Cloudflare WAF Firewall rules to block traffic. Using free lists like spamhaus, feodo, and firehol, allows any user to update their Cloudflare instance to block traffic to these known bad IPs.


Suggested lists (please research why each list exists and what they contain, some contain RFC1918 IPs):
- https://raw.githubusercontent.com/ktsaou/blocklist-ipsets/master/firehol_level1.netset
- https://www.spamhaus.org/drop/drop.txt
- https://www.spamhaus.org/drop/edrop.txt
- https://feodotracker.abuse.ch/downloads/ipblocklist_recommended.txt
- https://feodotracker.abuse.ch/downloads/ipblocklist.txt
