package ip

import (
	"fmt"
	"net"
	"strings"
)

// Validate determines if the input is an IP in the form of a host or cidr.
// It returns true if it fits either of these forms and returns the cleaned value.
// 10.0.0.1 -> returned as 10.0.0.1/32. Also removes blank space from string.
func Validate(input string) (string, bool) {
	// trim off any blank space first
	potentialIP := strings.TrimSpace(input)

	if !strings.Contains(potentialIP, "/") {
		// add /32 to ip if no cidr present
		potentialIP = fmt.Sprintf("%s/32", potentialIP)
	}

	_, _, err := net.ParseCIDR(potentialIP)
	if err != nil {
		return "", false
	}

	return potentialIP, true
}
