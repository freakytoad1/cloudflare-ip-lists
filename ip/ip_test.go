package ip

import "testing"

func TestValidate(t *testing.T) {
	tests := []struct {
		name  string
		input string
		want  string
		want1 bool
	}{
		{
			name:  "POSITIVE - good ip - cidr",
			input: "10.0.0.0/8",
			want:  "10.0.0.0/8",
			want1: true,
		},
		{
			name:  "POSITIVE - good ip - host",
			input: "10.0.0.1",
			want:  "10.0.0.1/32",
			want1: true,
		},
		{
			name:  "NEGATIVE - empty",
			input: "",
			want:  "",
			want1: false,
		},
		{
			name:  "NEGATIVE - bad ip - cidr",
			input: "10.300.0.0/8",
			want:  "",
			want1: false,
		},
		{
			name:  "NEGATIVE - bad ip - cidr2",
			input: "10.0.0.0/90",
			want:  "",
			want1: false,
		},
		{
			name:  "NEGATIVE - bad ip - host",
			input: "10.300.0.1",
			want:  "",
			want1: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1 := Validate(tt.input)
			if got != tt.want {
				t.Errorf("Validate() got = %v, want %v", got, tt.want)
			}
			if got1 != tt.want1 {
				t.Errorf("Validate() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}
