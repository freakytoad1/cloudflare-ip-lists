package sourcelist

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"

	"gitlab.com/freakytoad1/cloudflare-ip-lists/ip"
)

type Entry struct {
	IP          string
	Description string
}

func Retrieve(listURL string) ([]*Entry, error) {
	// request
	resp, err := http.Get(listURL)
	if err != nil {
		return nil, fmt.Errorf("unable to get source list: %w", err)
	} else if resp.StatusCode != 200 {
		return nil, fmt.Errorf("non OK error code returned: %d", resp.StatusCode)
	}

	// unmarshal
	defer resp.Body.Close()
	body, _ := ioutil.ReadAll(resp.Body)

	return parse(string(body))
}

func parse(input string) ([]*Entry, error) {
	if len(input) < 1 {
		return nil, fmt.Errorf("input was empty")
	}

	// expecting input to look like:
	// ; Spamhaus EDROP List 2022/10/19 - (c) 2022 The Spamhaus Project
	// ; https://www.spamhaus.org/drop/edrop.txt
	// ; Last-Modified: Wed, 21 Sep 2022 08:26:49 GMT
	// ; Expires: Thu, 20 Oct 2022 23:57:58 GMT
	// 2.56.58.0/24 ; SBL578519
	// 5.188.11.0/24 ; SBL402809
	// 27.112.32.0/19 ; SBL237955
	// 31.210.20.0/24 ; SBL545365

	// or

	// # firehol_level1
	// #
	// # ipv4 hash:net ipset
	// #
	// # A firewall blacklist composed from IP lists, providing
	// # maximum protection with minimum false positives. Suitable
	// # for basic protection on all internet facing servers,
	// # routers and firewalls. (includes: bambenek_c2 dshield feodo
	// # fullbogons spamhaus_drop spamhaus_edrop sslbl ransomware_rw)
	// #
	// # Maintainer      : FireHOL
	// #
	// 0.0.0.0/8
	// 1.10.16.0/20

	// new entry on new line
	// ';' or '#' marks start of comment to the end of that line

	result := []*Entry{}

	// we'll grab the very first char of the very first line as the 'comment' seperator
	seperator := string(input[0])
	log.Printf("using %q as seperator", seperator)

	for _, entry := range strings.Split(input, "\n") {
		if len(entry) < 1 {
			// skip the empty lines
			continue
		}

		entrySplit := strings.Split(entry, seperator)
		if len(entrySplit[0]) < 1 {
			// skip the entries that are only comments
			continue
		}

		potentialIP := entrySplit[0]
		description := ""
		if len(entrySplit) > 1 {
			// only add description if provided
			description = strings.TrimSpace(entrySplit[1])
		}

		if ipToAdd, valid := ip.Validate(entrySplit[0]); valid {
			// log.Printf("valid IP found: %q %q", ipToAdd, description)
			result = append(result, &Entry{
				IP:          ipToAdd,
				Description: description,
			})
			continue
		}

		log.Printf("entry %q is not a valid IP", potentialIP)
	}

	return result, nil
}
